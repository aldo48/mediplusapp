import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, StatusBar, Image, ScrollView, Dimensions} from 'react-native';
import { Header } from 'react-native-elements';
import Icons from '../../../Assets/Icons/icon_font';
import { color, font } from '../../Variabel/global_style';
import moment from 'moment';

var idLocale = require('moment/locale/id'); 
moment.locale('id', idLocale);
class DetailBerita extends Component {
  render() {
    const dataBerita = this.props.navigation.getParam("data")
    
    return (
      <ScrollView style={styles.container}>
        <StatusBar translucent backgroundColor="transparent" barStyle= {'dark-content'} />
        <Header
          leftComponent={
            <View style={styles.containerComponentHeader}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.iconBar}>
                  <Icons name="icon" size={18} color={color.white}/>
              </TouchableOpacity>
              <Text style={styles.textNamaBar}>Berita</Text>
            </View>
          }
          containerStyle={styles.containerHeader}
        />
        <View style={styles.containerBody}>
          <Image 
            source={{uri: dataBerita.urlToImage}}
            style={styles.imgBerita}
          /> 
          <Text style={styles.textTitle}>{dataBerita.title}</Text>
          <Text style={styles.textSubtitle}>Sumber : {dataBerita.source.name}</Text>
          <Text style={styles.textSubtitle}>{moment(dataBerita.publishedAt).format('dddd, D MMM YYYY, h:mm')}</Text>
          <Text style={styles.textKonten}>{dataBerita.content}</Text>
        </View>
        
      </ScrollView>
    );
  }
}

export default DetailBerita;

const screenWidth = Math.round(Dimensions.get('window').width);

const styles = StyleSheet.create({
  container:{
    flex:1,
  },
  containerBody:{
    flex:1,
    padding:20
  },

  // header
  containerComponentHeader:{
    backgroundColor: color.light_blue,
  },
  containerComponentHeader:{
    flexDirection:'row',
    alignItems: 'center'
  },
  iconBar:{
    padding:10
  },
  textNamaBar:{
    color: color.white,
    fontFamily: font.medium,
    fontSize: 20,
    marginLeft:10
  },
  
  //Konten Berita

  imgBerita:{
    flex:1,
    alignSelf: 'center',
    width: 378, 
    height:180,
    borderRadius:10,
    borderWidth:1,
    borderColor: color.dark_grey,
  },
  textTitle:{
    color: color.black,
    fontFamily: font.medium,
    fontSize: 18,
    marginTop:10,
    marginBottom:5
  },
  textSubtitle:{
    color: color.grey,
    fontFamily: font.reguler,
    fontSize: 10,
  },
  textKonten:{
    color: color.black,
    fontFamily: font.reguler,
    fontSize: 10,
    marginTop:10
  },
})