import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { color, font } from '../../Variabel/global_style';
import moment from 'moment';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { withNavigation } from 'react-navigation';

var idLocale = require('moment/locale/id'); 
moment.locale('id', idLocale);

class ItemBerita extends Component {
  constructor(props) {
    super(props);
    this.state = {
      
    };
  }

  render() {
    const dataBerita = this.props.item
    const id = this.props.index
    return (
      <View style={styles.container} key={"key_" + id}>
        <Image 
          source={{uri: dataBerita.urlToImage}}
          style={styles.imgBerita}
        />
        <Text style={styles.textTitle}>{dataBerita.title}</Text>
        <Text style={styles.textSubtitle}>Sumber : {dataBerita.source.name}</Text>
        <Text style={styles.textSubtitle}>{moment(dataBerita.publishedAt).format('dddd, D MMM YYYY, h:mm')}</Text>
        <Text style={styles.textDeskripsi}>{dataBerita.description}</Text>
        <TouchableOpacity 
          onPress= {() => this.props.navigation.navigate('detailBerita', { data: dataBerita }) }>
          <Text style={styles.textLinkToDetail}>Baca Selengkapnya...</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default withNavigation(ItemBerita) ;

const styles = StyleSheet.create({
  container:{
    flex:1,
  },

  imgBerita:{
    flex:1,
    alignSelf: 'center',
    width: 378, 
    height: 180,
    borderRadius:10,
    borderWidth:1,
    borderColor: color.dark_grey,
    marginTop: 20
  },
  textTitle:{
    color: color.black,
    fontFamily: font.medium,
    fontSize: 18,
    marginTop:10,
    marginBottom:5
  },
  textSubtitle:{
    color: color.grey,
    fontFamily: font.reguler,
    fontSize: 10,
  },
  textDeskripsi:{
    color: color.black,
    fontFamily: font.reguler,
    fontSize: 10,
    marginTop:10
  },
  textLinkToDetail:{
    color: color.light_blue,
    fontFamily: font.reguler,
    fontSize: 10,
    marginTop:10
  }
})