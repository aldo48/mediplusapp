import React, { Component } from 'react';
import { StyleSheet, 
  View, 
  Text, 
  TextInput,
  StatusBar, 
  RefreshControl, 
  ImageBackground, ScrollView, FlatList, TouchableOpacity, Dimensions} from 'react-native';
import { color, font } from '../../Variabel/global_style';
import LinearGradient from 'react-native-linear-gradient';
import Icons from '../../../Assets/Icons/icon_font.js';
import { FlatListSlider }  from './custom-slider';

import ItemBerita from '../Berita/ItemBerita';

class HomePage extends Component {
  constructor(props){
    super(props);
    this.state = {
      dataMenuGrid:[
        {
          name: 'hospital-buildings',
          desc: 'KLINIK TERDEKAT',
          color: color.blue,
        },
        {
          name: 'list',
          desc: 'RIWAYAT',
          color: color.light_blue,
        },
        {
          name: 'image-gallery',
          desc: 'DATA SCAN',
          color: color.purple
        },
        {
          name: 'notification',
          desc: 'NOTIFIKASI',
          color: color.light_blue,
        },
        {
          name: 'ic_grade_24px',
          desc: 'BERI NILAI',
          color: color.purple
        },
        {
          name: 'gear-option',
          desc: 'PENGATURAN',
          color: color.pink
        },
      ],
      dataSlider: [
        {
          image: require('../../../Assets/images/image1.jpg')
        },{
          image: require('../../../Assets/images/image1.jpg')
        },{
          image: require('../../../Assets/images/image1.jpg')
        },{
          image: require('../../../Assets/images/image1.jpg')
        },{
          image: require('../../../Assets/images/image1.jpg')
        }
      ],
      dataBerita: null,
      refreshing: false,
      loading: true,
    }
  }

  componentDidMount(){
    this.fetchDataBerita()
  }

  fetchDataBerita=()=>{   
    const url = `http://newsapi.org/v2/everything?q=bitcoin&from=2020-07-04&sortBy=publishedAt&apiKey=badf4342a6f341d1b456eb054754dd74`;
    fetch(url,{
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        }
    })
    .then(result => result.json())
    .then(result => {
      if(result.status=='ok'){
        this.setState({
          dataBerita : result.articles,
          refreshing: false,
          loading: false,
        }) 
      }else{ 
        this.setState({
          loading: false,
          refreshing: false,
        });
      }
    })
    .catch(error => {
      console.log(error)
    })    
  }

  renderItemMenuGrid = ({item,index}) => {
    return(
      <TouchableOpacity 
        key={"key_"+index}
        activeOpacity={.7} 
        style={{flex:1}}>
        <Icons 
          name= {item.name}
          size= {24}
          backgroundColor= {color.grey}
          style={{
            alignSelf:"center",
            padding: 20,
            color: color.white,
            backgroundColor: item.color,
            borderRadius: 20,
            margin: 30,
            marginBottom:10,
            marginTop:20
          }}
        /> 
        <Text style={styles.iconDesc}>{item.desc}</Text>
      </TouchableOpacity>
    )
  }
  
  render() {
    return (
      <View style={styles.container}>
        <StatusBar translucent backgroundColor="transparent" barStyle= {'dark-content'} />
        <View style={styles.header}>
          <Text style={styles.logo}>MEDIPLUS</Text>
          <Text style={styles.textName}>Hi, Bagus</Text>
          <Icons 
            name= "user"
            size= {22}
            color= {color.black}
            style= {styles.iconUser}
          />
        </View>

        <TextInput 
          style={styles.searchBar}
          placeholder="Cari Klinik / Rumah Sakit"
          underlineColorAndroid='transparent'
          onChangeText={() => {}}/>
        <ScrollView 
          showsVerticalScrollIndicator={false}
          onScroll={()=>{this.fetchDataBerita()}}
          refreshControl={
              <RefreshControl
                  refreshing={this.state.loading}
                  onRefresh={this.onRefreshstate}
              />
          }>
          <LinearGradient
            colors={[color.light_blue, color.pink]}
            start={{x: 0.0, y: 0.0}} end={{x: 1.0, y:1.0}}
            style={styles.containerInfoAntrian}>
              <Text style={styles.textInfoAntrean}>INFO ANTRIAN</Text>

              <View style={styles.line}/>

              <View style={{flexDirection:'row'}}>
                <View style={{flexDirection:'column'}}>
                  <ImageBackground
                  style={styles.imageBackground}
                  source={require('../../../Assets/images/favorites-button.png')}>
                    <Text style={styles.jumlahNomorAntrian}>21</Text>
                  </ImageBackground>
                  <Text style={styles.textAntrian}>Nomor antrian</Text>
                </View>

                <View style={{flexDirection:'column'}}>
                  <ImageBackground
                    style={styles.imageBackground1}
                    source={require('../../../Assets/images/favorites-button.png')}>
                    <Text style={styles.jumlahSisaAntrian}>5</Text>
                  </ImageBackground>
                  <Text style={styles.textAntrian}>Sisa antrian</Text>
                </View>
                
                <View style={styles.containerInfoDokter}>
                  <Text style={styles.textProfile}>Dokter anda</Text>
                  <Text style={styles.textNama}>dr.Rina Agustina</Text>
                  <Text style={styles.textProfile}>Klinik/RS Anda</Text>
                  <Text style={styles.textNama}>RS. National Hospital</Text>
                </View>
              </View>
          </LinearGradient>

          <FlatList
            data={this.state.dataMenuGrid}
            renderItem={this.renderItemMenuGrid}
            numColumns={3}
            keyExtractor={(item,index) => "key_"+index}
          />

          <FlatListSlider 
            data={this.state.dataSlider} 
            timer={5000}
            indicatorActiveWidth={40}
            contentContainerStyle={{marginTop:20,}}
            local
          />

          <FlatList
            data={this.state.dataBerita}
            renderItem={({item,index}) => {
                  return(
                    <ItemBerita item={item} index={index}/>
                  )
                }
              }
            keyExtractor={(item,index) => "key_"+index}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: color.white,
    padding: 16 
  },

  //Header
  header: {
    flexDirection: 'row',
    alignContent: 'center',
    marginTop: 30
  },
  logo:{
    padding: 10,
    fontSize: 19,
    fontFamily: font.medium,
    color: color.black,
  },
  iconUser:{
    marginTop:10,
    marginLeft: 10
  },
  textName: {
    paddingTop: 14,
    marginLeft: 'auto',
    fontSize: 12,
    fontFamily: font.medium,
    color: color.black,
  },

  //Search Bar

  searchBar:{
    color: color.black,
    borderColor: color.light_grey,
    backgroundColor: color.soft_grey,
    borderRadius:30,
    borderWidth: 1,
    marginTop: 10,
    marginBottom:20,
    paddingLeft: 20,
    paddingRight: 20,
  },

  scrollView:{
    backgroundColor: color.light_grey
  },

  //StatusInfo
  containerInfoAntrian:{
    // padding:10,
    borderRadius:10,
  },
  textInfoAntrean: {
    color: color.white,
    fontFamily: font.bold,
    fontSize: 12,
    padding:10,
    marginLeft: 10
  },

  line:{
    borderBottomColor: color.white,
    borderBottomWidth: 2,
    marginBottom:10,
  },
  imageBackground:{
    flexDirection:'column',
    alignContent: 'center',
    marginLeft: 10,
  },
  imageBackground1:{
    flexDirection:'column',
    alignContent: 'center',
    marginLeft: 10,
    transform: [{ rotate: '180deg' }]
  },
  jumlahNomorAntrian:{
    alignSelf: 'center',
    color: color.white,
    fontFamily: font.medium,
    fontSize: 25,
    margin: 35,
  },
  jumlahSisaAntrian:{
    alignSelf: 'center',
    color: color.white,
    fontFamily: font.medium,
    fontSize: 25,
    margin: 35,
    transform: [{ rotate: '180deg' }]
  },
  textAntrian:{
    color: color.white,
    fontFamily: font.bold,
    alignSelf: 'center',
    fontSize: 9,
    marginLeft:10,
    marginTop:-10,
    marginBottom:20
  },
  containerInfoDokter:{
    flexDirection:'column',
    margin:10
  },
  textProfile:{
    color: color.light_yellow,
    fontFamily: font.medium,
    fontSize: 9,
    padding:5
  },
  textNama:{
    color: color.white,
    fontFamily: font.medium,
    fontSize: 9,
    padding:5
  },

  // Grid Menu
 
  iconDesc:{
    alignSelf: 'center',
    color: color.black,
    fontFamily: font.medium,
    fontSize:10
  },

  //Slider

  contentStyle:{
    flex:1
  }
  
})

export default HomePage;
