import React from 'react';
import {TouchableOpacity, Image, StyleSheet, Dimensions} from 'react-native';

export default (ChildItem = ({
  item,
  style,
  onPress,
  index,
  imageKey,
  local,
  height
}) => {
  return (
    <TouchableOpacity
      style={styles.container}
      >
      <Image
        style={styles.image}
        source={local ? item[imageKey] : {uri: item[imageKey]}}
      />
    </TouchableOpacity>
  );
});

const styles = StyleSheet.create({
  container: {
    margin:5
  },
  image: {
    height:170,
    width: Math.round(Dimensions.get('window').width)-10,
    borderRadius:10
  },
});
