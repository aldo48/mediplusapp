import {createAppContainer} from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack';
import HomePage from '../Pages/Home/HomePage';
import DetailBerita from '../Pages/Berita/DetailBerita';

const Navigation = createStackNavigator({
    Home : {
        screen : HomePage
    }, 
    detailBerita : {
        screen : DetailBerita
    }
},{
    initialRouteName: 'Home',
    headerMode : 'none',
});

export default createAppContainer(Navigation);
