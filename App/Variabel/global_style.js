// Style Color

export const color = {
    black: '#5A5A5A',
    white: '#ffffff',
    grey: '#B7B7B7',
    dark_grey: '#707070',
    light_grey: '#E3E3E3',
    soft_grey: '#F4F4F4',
    light_blue: '#0093DD',
    blue: '#277CCC',
    pink: '#DD127B',
    light_yellow: '#FFF9AA',
    purple: '#8445A2'
}

// Style Font

export const font = {
    black:'Rubik-Black',
    bold:'Rubik-Bold',
    book:'Rubik-Book',
    extrabold:'Rubik-ExtraBold',
    light:'Rubik-Light',
    medium:'Rubik-Medium',
    reguler: 'Rubik-Regular'
}