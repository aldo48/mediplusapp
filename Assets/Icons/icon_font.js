import React from 'react';
import {View} from 'react-native';
import { createIconSetFromFontello } from "react-native-vector-icons";

import IconFontConfig from "./config.json";
const IconFont = createIconSetFromFontello(IconFontConfig);

const Icon = (props) => {
    return (
        <View>
            <IconFont
                color={props.color}
                size={props.size}
                name={props.name}
                style={props.style}
            />
        </View>
    )
}

export default Icon;